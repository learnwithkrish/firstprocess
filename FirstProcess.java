class FirstProcess{
	
	FirstProcess(){
		System.out.println("I am Constructor 1");
	}
	
	static{
		System.out.println("I am Static block 1");
	}
	
	{
		System.out.println("I am normal block 1");
	}
	
	static{
		System.out.println("I am Static block 2");
	}
	
	{
		System.out.println("I am normal block 2");
	}
	
	FirstProcess(String m){
		System.out.println("I am Constructor 2");
	}
	
	public static void main(String [] args){
		FirstProcess firstProcess = new FirstProcess();
		FirstProcess firstProcess1 = new FirstProcess("init");
	}
}