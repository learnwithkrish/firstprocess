This program is to check which order it will run the block.
* Constructors Block
* Static Block
* Normal Block

After the code ran. I found the order and when it was running.

//constructor 1
FirstProcess(){
		System.out.println("I am Constructor 1");
	}
	
	//static block 1
	static{
		System.out.println("I am Static block 1");
	}
	
	//normal block 1
	{
		System.out.println("I am normal block 1");
	}
	
	//static block 2
	static{
		System.out.println("I am Static block 2");
	}
	
	//normal block 2
	{
		System.out.println("I am normal block 2");
	}
	
	//constructor 2
	FirstProcess(String m){
		System.out.println("I am Constructor 2");
	}

I created objects for above class in the main method.

		FirstProcess firstProcess = new FirstProcess();
		FirstProcess firstProcess1 = new FirstProcess("init");
		
It produce following outputs

I am Static block 1
I am Static block 2
I am normal block 1
I am normal block 2
I am Constructor 1
I am normal block 1
I am normal block 2
I am Constructor 2

Using above output. I concluded some points about blocks and constructors.

*Static blocks are running when class(FirstProcess class) is once loaded in JVM.

*Normal blocks are running it's wrote order each time when object created for that class and before constructors

*Constructor are excuted when object are created.